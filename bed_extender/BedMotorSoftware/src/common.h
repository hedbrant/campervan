#ifndef COMMON_H
#define COMMON_H

enum State{
	INIT,
	IDLE,
	MOVING,
	JOGGING
};

struct Error{
	enum ErrorType{
		ERR_OK,
		ERR_OVER_CURRENT
	} error_type;
};

enum Dir{
	DIR_NONE,
	DIR_OUT,
	DIR_IN
};

#endif