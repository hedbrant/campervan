#include "motor_control.h"

struct MotorData {
	uint16_t current_l = 0;
	uint16_t current_r = 0;
} motor_data;

volatile uint8_t cnt = 0;
volatile bool led_state = false;
volatile bool motor_l_in1 = true;
volatile bool motor_l_in2 = false;
volatile bool motor_r_in1 = true;
volatile bool motor_r_in2 = false;

float crnt, crnt_l_filt, crnt_r_filt;

void init_motor_control(){
	pinMode(MOTOR_L_IN1_PIN, OUTPUT);
	pinMode(MOTOR_L_IN2_PIN, OUTPUT);
	pinMode(MOTOR_R_IN1_PIN, OUTPUT);
	pinMode(MOTOR_R_IN2_PIN, OUTPUT);

	pinMode(CURRENT_L_PIN, INPUT);
	pinMode(CURRENT_R_PIN, INPUT);
}

float calc_current_ma(uint16_t current_raw, int zero_point){
	return - 1000.0 * ((int) current_raw - zero_point) * (5.0 / (0.185 * 1024.0));
}

void read_current(){
	//data->current_l = analogRead(CURRENT_L);
	//data->current_r = analogRead(CURRENT_R);
	cnt++;
	if (cnt % 20 == 0){
		crnt = calc_current_ma(analogRead(CURRENT_L_PIN), 515);
		crnt_l_filt = crnt * 0.05 + crnt_l_filt * 0.95;
		Serial.print(">current_l:");
		Serial.println(crnt);
		Serial.print(">current_l_filtered:");
		Serial.println(crnt_l_filt);

		crnt = calc_current_ma(analogRead(CURRENT_R_PIN), 516);
		crnt_r_filt = crnt * 0.05 + crnt_r_filt * 0.95;
		Serial.print(">current_r:");
		Serial.println(crnt);
		Serial.print(">current_r_filtered:");
		Serial.println(crnt_r_filt);
	}
	if (cnt >= 100){
		cnt = 0;
		//motor_in1 = !motor_in1;
		//motor_in2 = !motor_in2;
	}
	digitalWrite(MOTOR_L_IN1_PIN, motor_l_in1);
	digitalWrite(MOTOR_L_IN2_PIN, motor_l_in2);
	digitalWrite(MOTOR_R_IN1_PIN, motor_r_in1);
	digitalWrite(MOTOR_R_IN2_PIN, motor_r_in2);
	digitalWrite(LED_BUILTIN, motor_l_in1);
}

void move_motors(uint8_t dir){
	;
}