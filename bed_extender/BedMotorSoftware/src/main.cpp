#include "Arduino.h"
#include "common.h"
#include "state_machine.h"
#include "motor_control.h"
#include "buttons.h"

// Outputs
#define LED_RED_PIN		A3
#define LED_BLUE_PIN	A4
#define LED_GREEN_PIN	A5

// --- Set system clock
void init_system_clock(){
	// 8 MHz
	CLKPR = (1 << CLKPCE);
	CLKPR = (0 << CLKPCE);
}

// --- Motor current reading
volatile bool timer2_ovf_flag = false;

void init_timer2(){
	// TODO: verify all of this
	TCCR2A = 0;	// set entire TCCR0A register to 0
	TCCR2B = 0;	// same for TCCR0B
	TCNT2  = 0;	// initialize counter value to 0
 	OCR2A = 0;	// output compare initialized to 0
	// System clock freq / (prescaler * timer reg length) = interrupt freq
	// (8 000 000 / (256 * 256)) = 122
	// Select Fast PWM mode
	TCCR2A |= (1 << WGM21) | (1 << WGM20);
	// Set prescaler to 256
	TCCR2B |= (1 << CS22) | (1 << CS21);
	// Enable timer overflow interrupt
	TIMSK2 |= (1 << TOIE2);
}

ISR(TIMER2_OVF_vect){
   timer2_ovf_flag = true;
}


// --- Control panel input


void setup() {
	cli();

	Serial.begin(115200);

	// TODO: verify all
	init_system_clock();

	init_timer2();

	pinMode(LED_BUILTIN, OUTPUT);

	// Set analog input pin modes
  	/*pinMode(CURRENT_L, INPUT);
  	pinMode(CURRENT_R, INPUT);*/
	sei();
}

unsigned long t_now;
unsigned long t_btn_prev;
unsigned long t_state_machine_prev;

void loop() {
	// ---- READINGS ----
	// Read motor currents - use ADC with timer triggered interrupt?
	if(timer2_ovf_flag){
		timer2_ovf_flag = false;
		read_current();
	}
	
	// Read button inputs - use pin change interrupts or polling?
	t_now = micros();
	if(t_now - t_btn_prev >= T_BTN_SAMPLE){
		t_btn_prev = t_now;
		//read_btn_inputs();
	}

	// ---- CONTROL ----
	// State machine for controller - switch case based on button inputs.
	t_now = micros();
	if(t_now - t_state_machine_prev >= T_STATE_MACHINE){
		t_state_machine_prev = t_now;
		//run_state_machine();
	}

	// ---- WRITE ----
	// control LEDs - use simple digital outputs.


	// control motors - step 1 - use PWM with 0/255 as outputs, step 2 - control based on position difference feedback.
}