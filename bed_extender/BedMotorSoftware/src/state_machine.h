#ifndef STATE_MACHINE_H
#define STATE_MACHINE_H

#include "common.h"

#define T_STATE_MACHINE		20000

void init_state_machine(State* p_state);
void run_state_machine();
void change_state(uint8_t new_state);

#endif