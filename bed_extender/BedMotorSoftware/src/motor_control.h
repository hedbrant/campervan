#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

#include "Arduino.h"

// TODO: set
#define MOTOR_L_IN1_PIN		9
#define MOTOR_L_IN2_PIN		3
#define MOTOR_R_IN1_PIN		11
#define MOTOR_R_IN2_PIN		10

#define CURRENT_L_PIN		A7
#define CURRENT_R_PIN		A6

// Current measurement
#define CURRENT_1_ZERO		515
#define CURRENT_1_K			- (5 / (0.185 * 1024))

void init_motor_control();
void read_current();
void move_motors(uint8_t dir);

#endif