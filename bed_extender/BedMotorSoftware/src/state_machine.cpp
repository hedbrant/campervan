#include "Arduino.h"
#include "state_machine.h"

State state = INIT;
Error error = { .error_type = Error::ERR_OK};

Dir jog_l_dir = DIR_NONE;
Dir jog_r_dir = DIR_NONE;
Dir move_dir = DIR_NONE;

void change_state(State new_state){
	Serial.println("State change");
	state = new_state;
}

void run_state_machine(){
	switch(state){
		case INIT:
			change_state(IDLE);
			break;
		case IDLE:
			;
			break;
		case MOVING:
			//move_motors(move_dir);
			break;
		case JOGGING:
			;
			break;
	}
}