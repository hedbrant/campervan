#include "buttons.h"
#include "state_machine.h"

using namespace ace_button;

ButtonConfig config[BTN_LENGTH];

AceButton btn_l_minus(&config[BTN_L_MINUS]);
AceButton btn_l_plus(&config[BTN_L_PLUS]);
AceButton btn_r_minus(&config[BTN_R_MINUS]);
AceButton btn_r_plus(&config[BTN_R_PLUS]);
AceButton btn_extend(&config[BTN_EXTEND]);
AceButton btn_retract(&config[BTN_RETRACT]);
AceButton btn_stop(&config[BTN_STOP]);
AceButton btn_reset(&config[BTN_RESET]);

AceButton* button[BTN_LENGTH];

uint8_t btn_pin[BTN_LENGTH];

State* state_p;
Error* error_p;
Dir	*jog_l_dir_p, *jog_r_dir_p, *move_dir_p;

void handle_event_l_minus(AceButton* button, uint8_t, uint8_t);
void handle_event_l_plus(AceButton* button, uint8_t, uint8_t);
void handle_event_r_minus(AceButton* button, uint8_t, uint8_t);
void handle_event_r_plus(AceButton* button, uint8_t, uint8_t);
void handle_event_extend(AceButton* button, uint8_t, uint8_t);
void handle_event_retract(AceButton* button, uint8_t, uint8_t);
void handle_event_stop(AceButton* button, uint8_t, uint8_t);
void handle_event_reset(AceButton* button, uint8_t, uint8_t);

void init_buttons(State* p_state, Error* p_error, Dir* p_jog_l_dir, Dir* p_jog_r_dir, Dir* p_move_dir){
	state_p = p_state;
	error_p = p_error;
	jog_l_dir_p = p_jog_l_dir;
	jog_r_dir_p = p_jog_r_dir;
	move_dir_p = p_move_dir;

	button[0] = &btn_l_minus;
	button[1] = &btn_l_plus;
	button[2] = &btn_r_minus;
	button[3] = &btn_r_plus;
	button[4] = &btn_extend;
	button[5] = &btn_retract;
	button[6] = &btn_stop;
	button[7] = &btn_reset;

	btn_pin[BTN_L_MINUS] 	= BTN_L_MINUS_PIN;
	btn_pin[BTN_L_PLUS] 	= BTN_L_PLUS_PIN;
	btn_pin[BTN_R_MINUS] 	= BTN_R_MINUS_PIN;
	btn_pin[BTN_R_PLUS] 	= BTN_R_PLUS_PIN;
	btn_pin[BTN_EXTEND] 	= BTN_EXTEND_PIN;
	btn_pin[BTN_RETRACT] 	= BTN_RETRACT_PIN;
	btn_pin[BTN_STOP] 		= BTN_STOP_PIN;
	btn_pin[BTN_RESET] 		= BTN_RESET_PIN;

	for(uint8_t i = 0; i < BTN_LENGTH; i++){
		pinMode(btn_pin[i], INPUT_PULLUP);
		button[i]->init(btn_pin[i]);
	}

	config[BTN_L_MINUS].setEventHandler(handle_event_l_minus);
	config[BTN_L_PLUS].setEventHandler(handle_event_l_plus);
	config[BTN_R_MINUS].setEventHandler(handle_event_r_minus);
	config[BTN_R_PLUS].setEventHandler(handle_event_r_plus);
	config[BTN_EXTEND].setEventHandler(handle_event_extend);
	config[BTN_RETRACT].setEventHandler(handle_event_retract);
	config[BTN_STOP].setEventHandler(handle_event_stop);
	config[BTN_RESET].setEventHandler(handle_event_reset);

	for(uint8_t i = 0; i < BTN_LENGTH; i++){
		config[i].setFeature(ButtonConfig::kFeatureClick);
	}
}

void read_btn_inputs(){
	for(uint8_t i = 0; i < BTN_LENGTH; i++){
		button[i]->check();
	}
}

void handle_event_l_minus(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventPressed){
		switch(*state_p){
			case IDLE:
				*jog_l_dir_p = DIR_IN;
				change_state(JOGGING);
				break;
			case JOGGING:
				if(*jog_l_dir_p == DIR_NONE){
					*jog_l_dir_p = DIR_IN;
				}
				break;
		}
	} else if (event_type == AceButton::kEventReleased){
		switch(*state_p){
			case JOGGING:
				if(*jog_r_dir_p == DIR_NONE){
					change_state(IDLE);
				}
				break;
		}
	}
}
void handle_event_l_plus(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventPressed){
		switch(*state_p){
			case IDLE:
				*jog_l_dir_p = DIR_OUT;
				change_state(JOGGING);
				break;
			case JOGGING:
				if(*jog_l_dir_p == DIR_NONE){
					*jog_l_dir_p = DIR_OUT;
				}
				break;
		}
	} else if (event_type == AceButton::kEventReleased){
		switch(*state_p){
			case JOGGING:
				if(*jog_r_dir_p == DIR_NONE){
					change_state(IDLE);
				}
				break;
		}
	}
}
void handle_event_r_minus(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventPressed){
		switch(*state_p){
			case IDLE:
				*jog_r_dir_p = DIR_IN;
				change_state(JOGGING);
				break;
			case JOGGING:
				if(*jog_r_dir_p == DIR_NONE){
					*jog_r_dir_p = DIR_IN;
				}
				break;
		}
	} else if (event_type == AceButton::kEventReleased){
		switch(*state_p){
			case JOGGING:
				if(*jog_l_dir_p == DIR_NONE){
					change_state(IDLE);
				}
				break;
		}
	}
}
void handle_event_r_plus(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventPressed){
		switch(*state_p){
			case IDLE:
				*jog_r_dir_p = DIR_OUT;
				change_state(JOGGING);
				break;
			case JOGGING:
				if(*jog_r_dir_p == DIR_NONE){
					*jog_r_dir_p = DIR_OUT;
				}
				break;
		}
	} else if (event_type == AceButton::kEventReleased){
		switch(*state_p){
			case JOGGING:
				if(*jog_l_dir_p == DIR_NONE){
					change_state(IDLE);
				}
				break;
		}
	}
}
void handle_event_extend(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventClicked){
		switch(*state_p){
			case IDLE:
				*move_dir_p = DIR_OUT;
				change_state(MOVING);
				break;
			case MOVING:
				if(*move_dir_p != DIR_OUT){
					*move_dir_p = DIR_OUT;
				}
				break;
		}
	}
}
void handle_event_retract(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventClicked){
		switch(*state_p){
			case IDLE:
				*move_dir_p = DIR_IN;
				change_state(MOVING);
				break;
			case MOVING:
				if(*move_dir_p != DIR_IN){
					*move_dir_p = DIR_IN;
				}
				break;
		}
	}
}
void handle_event_stop(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventPressed){
		change_state(IDLE);
	}
}
void handle_event_reset(AceButton* button, uint8_t event_type, uint8_t button_state){
	if(event_type == AceButton::kEventClicked){
		error_p->error_type = Error::ERR_OK;
		if(*state_p != IDLE){
			change_state(IDLE);
		}
	}
}