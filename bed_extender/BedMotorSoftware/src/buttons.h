#ifndef BUTTONS_H
#define BUTTONS_H

#include "AceButton.h"

#define T_BTN_SAMPLE 		2000

// TODO: set
#define BTN_L_PLUS_PIN 		13
#define BTN_L_MINUS_PIN		12
#define BTN_R_PLUS_PIN		8
#define BTN_R_MINUS_PIN		7
#define BTN_EXTEND_PIN		6
#define BTN_RETRACT_PIN		5
#define BTN_STOP_PIN		4
#define BTN_RESET_PIN		2

struct Button{
	uint8_t pin;
	bool state;
	bool prev_state;
};

enum BtnEnum {
	BTN_L_PLUS,
	BTN_L_MINUS,
	BTN_R_PLUS,
	BTN_R_MINUS,
	BTN_EXTEND,
	BTN_RETRACT,
	BTN_STOP,
	BTN_RESET,
	BTN_LENGTH
};

void read_btn_inputs();

#endif