

def main():

	""" force = 4000
	dist = 0.55
	moment = force * dist
	print('moment: ', moment, ' Nm')

	t = 0.002
	B = 0.015
	H = 0.03
	b = B - 2*t
	h = H - 2*t
	Wb_tot = (B*H*H / 6) - (b*h*h*h / (6*H)) """
	
	force = 1700
	dist = 0.5
	moment = force * dist
	print('moment: ', moment, ' Nm')

	t = 0.003
	B = 0.025
	H = 0.04
	b = B - 2*t
	h = H - 2*t
	Wb_tot = 2 * ((B*H*H / 6) - (b*h*h*h / (6*H)))

	max_stress = moment / Wb_tot
	print('max stress: ', round(max_stress/1e6), ' MPa')

if __name__ == '__main__':
	main()